<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccountController
 * @Route("account")
 * @package AppBundle\Controller
 */
class AccountController extends BaseController
{
    /**
     * @Route("/")
     * @Method({"GET","POST"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository(Post::class)
            ->findBy(['author'=>$this->getUser()->getId()],['datetime'=>'desc']);

        return $this->renderOutput('@App/account/index.html.twig', [
            'listPost'=>$posts
        ]);
    }

    /**
     * @Route("/edit")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction()
    {
        return $this->renderOutput('@App/account/edit.html.twig');
    }

    /**
     * @Route("/photo")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function photoAction()
    {
        return $this->renderOutput('@App/account/photo.html.twig');
    }

    /**
     * @Route("/friends")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function friendsAction()
    {
        $myFriends = $this->getUser()->getMyFriends();
        return $this->renderOutput('@App/account/friends.html.twig',[
            'myFriends' => $myFriends
        ]);
    }

    /**
     * @Route("/add_friend")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFriendAction(Request $request)
    {
        $userCurrent = $this->getUser();
        $userId = intval($request->get('user_id'));

        $status = false;
        $message = "Добавлен!";

        try {

            $user = $this
                ->getDoctrine()
                ->getRepository(User::class)
                ->find($userId);

            if (!$user) {
                throw new \Exception("Такого пользователя не найденно!");
            }

            $user->addFriendsWithMe($userCurrent);
            $userCurrent->addMyFriend($user);

            $em = $this
                ->getDoctrine()
                ->getManager();

            $em->flush();
            $status = true;

        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'success' => $status,
            'message' => $message
        ]);

    }

    /**
     * @Route("/remove_friend")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeFriendAction(Request $request)
    {
        $userCurrent = $this->getUser();
        $userId = intval($request->get('user_id'));

        $status = false;
        $message = "удален!";

        try {

            $user = $this
                ->getDoctrine()
                ->getRepository(User::class)
                ->find($userId);

            if (!$user) {
                throw new \Exception("Такого пользователя не найденно!");
            }

            $user->removeFriendsWithMe($userCurrent);
            $userCurrent->removeMyFriend($user);

            $em = $this
                ->getDoctrine()
                ->getManager();

            $em->flush();
            $status = true;

        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'success' => $status,
            'message' => $message
        ]);

    }

    /**
     * @Route("/add_post")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPostAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $post->setAuthor($this->getUser());
            $post->setDatetime(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();
        }

        return $this->redirectToRoute('app_home_index');
    }

    /**
     * @Route("/add_post_like")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPostLikeAction(Request $request)
    {
        $userCurrent = $this->getUser();
        $postId = intval($request->get('post_id'));
        $countLike = 0;
        $status = false;
        $message = "Добавлен!";

        try {

            $post = $this
                ->getDoctrine()
                ->getRepository(Post::class)
                ->find($postId);

            if (!$post) {
                throw new \Exception("Такого поста не найденно!");
            }

            $post->addLike($userCurrent);
            $userCurrent->addLikePost($post);

            $em = $this
                ->getDoctrine()
                ->getManager();

            $em->flush();
            $countLike = $post->getLike()->count();
            $status = true;

        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'success' => $status,
            'message' => $message,
            'countLike' => $countLike
        ]);

    }
    /**
     * @Route("/remove_post_like")
     * @Method({"POST"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeLikeAction(Request $request)
    {
        $userCurrent = $this->getUser();
        $postId = intval($request->get('post_id'));
        $countLike = 0;

        $status = false;
        $message = "Удален!";

        try {

            $post = $this
                ->getDoctrine()
                ->getRepository(Post::class)
                ->find($postId);

            if (!$post) {
                throw new \Exception("Такого поста не найденно!");
            }

            $post->removeLike($userCurrent);
            $userCurrent->removeLikePost($post);

            $em = $this
                ->getDoctrine()
                ->getManager();

            $em->flush();
            $countLike = $post->getLike()->count();
            $status = true;

        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'success' => $status,
            'message' => $message,
            'countLike' => $countLike
        ]);

    }
}