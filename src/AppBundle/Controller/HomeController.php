<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HomeController extends BaseController
{
    /**
     * @Route("/")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $posts = $this
            ->getDoctrine()
            ->getRepository(Post::class)
            ->findBy([],['datetime'=>'desc']);

        return $this->renderOutput('@App/home/index.html.twig',[
            'listPost'=>$posts
        ]);
    }

    /**
     * @Route("/peoples")
     * @Method({"GET"})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function peoplesAction()
    {
        return $this->renderOutput('@App/home/peoples.html.twig');
    }


}
