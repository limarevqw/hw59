<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends Controller
{
    protected function renderOutput($view, array $parameters = array(), Response $response = null)
    {
        $userId = $this->getUser()->getId();
        $userRepository = $this
            ->getDoctrine()
            ->getRepository(User::class);

        $form = $this->createForm(PostType::class, new Post());

        $data = [
            'peoples_sitebar' => $userRepository->findAllPeoples($userId),
            'peoples' => $userRepository->findAllPeoples($userId,100),
            'formPost' => $form->createView()
        ];

        $parameters = array_merge($parameters,$data);

        return $this->render($view,$parameters,$response);
    }
}