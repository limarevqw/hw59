<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="text", nullable=true, unique=false)
     */
    private $avatar;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    private $avatarFile;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="myFriends")
     */
    protected $friendsWithMe;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="friendsWithMe")
     * @ORM\JoinTable(name="users_friends",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_user_id", referencedColumnName="id")}
     *      )
     */
    protected $myFriends;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="author")
     */
    protected $commentaries;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     */
    protected $posts;

    /**
     * @ORM\ManyToMany(targetEntity="Post", inversedBy="like")
     * @ORM\JoinTable(name="like_posts",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="post_id", referencedColumnName="id")}
     *     )
     */
    protected $likePosts;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="author")
     */
    protected $photos;


    public function __construct()
    {
        parent::__construct();

        $this->friendsWithMe = new ArrayCollection();
        $this->myFriends = new ArrayCollection();
        $this->likePosts = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     *
     * @param File|UploadedFile $image
     *
     * @return User
     */
    public function setAvatarFile(File $image = null)
    {
        $this->avatarFile = $image;
        return $this;
    }
    /**
     * @return File|null
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }
    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }
    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param User[] $friendsWithMe
     * @return User
     */
    public function setFriendsWithMe($friendsWithMe)
    {
        $this->friendsWithMe = $friendsWithMe;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getFriendsWithMe()
    {
        return $this->friendsWithMe;
    }

    /**
     * @param User $myFriends
     * @return User
     */
    public function setMyFriends($myFriends)
    {
        $this->myFriends = $myFriends;
        return $this;
    }

    /**
     * @return User[]
     */
    public function getMyFriends()
    {
        return $this->myFriends;
    }

    /**
     * @param Comment[] $commentaries
     * @return User
     */
    public function setCommentaries($commentaries)
    {
        $this->commentaries = $commentaries;
        return $this;
    }

    /**
     * @return Comment[]
     */
    public function getCommentaries()
    {
        return $this->commentaries;
    }

    /**
     * @param Post[] $posts
     * @return User
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
        return $this;
    }

    /**
     * @return Post[]
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $likePosts
     * @return User
     */
    public function setLikePosts($likePosts)
    {
        $this->likePosts = $likePosts;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLikePosts()
    {
        return $this->likePosts;
    }

    /**
     * @param Photo[] $photos
     * @return User
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
        return $this;
    }

    /**
     * @return Photo[]
     */
    public function getPhotos()
    {
        return $this->photos;
    }


    /**
     * Add friendsWithMe
     *
     * @param \AppBundle\Entity\User $friendsWithMe
     *
     * @return User
     */
    public function addFriendsWithMe(\AppBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe[] = $friendsWithMe;

        return $this;
    }

    /**
     * Remove friendsWithMe
     *
     * @param \AppBundle\Entity\User $friendsWithMe
     */
    public function removeFriendsWithMe(\AppBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe->removeElement($friendsWithMe);
    }

    /**
     * Add myFriend
     *
     * @param \AppBundle\Entity\User $myFriend
     *
     * @return User
     */
    public function addMyFriend(\AppBundle\Entity\User $myFriend)
    {
        $this->myFriends[] = $myFriend;

        return $this;
    }

    /**
     * Remove myFriend
     *
     * @param \AppBundle\Entity\User $myFriend
     */
    public function removeMyFriend(\AppBundle\Entity\User $myFriend)
    {
        $this->myFriends->removeElement($myFriend);
    }

    /**
     * Add commentary
     *
     * @param \AppBundle\Entity\Comment $commentary
     *
     * @return User
     */
    public function addCommentary(\AppBundle\Entity\Comment $commentary)
    {
        $this->commentaries[] = $commentary;

        return $this;
    }

    /**
     * Remove commentary
     *
     * @param \AppBundle\Entity\Comment $commentary
     */
    public function removeCommentary(\AppBundle\Entity\Comment $commentary)
    {
        $this->commentaries->removeElement($commentary);
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return User
     */
    public function addPost(\AppBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Post $post
     */
    public function removePost(\AppBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Add likePost
     *
     * @param \AppBundle\Entity\Post $likePost
     *
     * @return User
     */
    public function addLikePost(\AppBundle\Entity\Post $likePost)
    {
        $this->likePosts[] = $likePost;

        return $this;
    }

    /**
     * Remove likePost
     *
     * @param \AppBundle\Entity\Post $likePost
     */
    public function removeLikePost(\AppBundle\Entity\Post $likePost)
    {
        $this->likePosts->removeElement($likePost);
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     *
     * @return User
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }
}
