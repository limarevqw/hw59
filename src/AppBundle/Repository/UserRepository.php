<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function findAllPeoples($currentUserId, $limit = 6)
    {
        return $this->createQueryBuilder('u')
            ->orderBy('u.id', 'desc')
            ->where('u.id != :id')
            ->setMaxResults($limit)
            ->setParameter("id",$currentUserId)
            ->getQuery()
            ->getResult();
    }
}
